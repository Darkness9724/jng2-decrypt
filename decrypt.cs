using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

class StringCipher {
    private static Rfc2898DeriveBytes _pdb;
    private static Aes encryptor;
    private static string[] _lines;
    public string[] LineArray => _lines;
    public static bool IsInComment;

    public static void SetPassword (string password) {
        _pdb = new Rfc2898DeriveBytes (password, new byte[13] {
            73,
            118,
            97,
            110,
            32,
            77,
            101,
            100,
            118,
            101,
            100,
            101,
            118
        });
        encryptor = Aes.Create ();
        encryptor.Key = _pdb.GetBytes (32);
        encryptor.IV = _pdb.GetBytes (16);
    }

    public static string Encrypt (string clearText) {
        byte[] bytes = Encoding.Unicode.GetBytes (clearText);
        using (MemoryStream memoryStream = new MemoryStream ()) {
            using (CryptoStream cryptoStream = new CryptoStream (memoryStream, encryptor.CreateEncryptor (), CryptoStreamMode.Write)) {
                cryptoStream.Write (bytes, 0, bytes.Length);
                cryptoStream.Close ();
            }
            clearText = Convert.ToBase64String (memoryStream.ToArray ());
            return clearText;
        }
    }

    public static string Decrypt (string cipherText) {
        cipherText = cipherText.Replace (" ", "+");
        byte[] array;
        try {
            array = Convert.FromBase64String (cipherText);
        } catch (Exception ex) {
            System.Console.WriteLine ("Unable to decrypt text: '{0}'. {1}", cipherText, ex.ToString ());
            return cipherText;
        }
        using (MemoryStream memoryStream = new MemoryStream ()) {
            using (CryptoStream cryptoStream = new CryptoStream (memoryStream, encryptor.CreateDecryptor (), CryptoStreamMode.Write)) {
                cryptoStream.Write (array, 0, array.Length);
                cryptoStream.Close ();
            }
            cipherText = Encoding.Unicode.GetString (memoryStream.ToArray ());
            return cipherText;
        }
    }
    
	public static void SetLines(string[] lines, string fileName, bool encoded = false) {
		_lines = lines;
        try {
            if (_lines?.Length > 0 && _lines[0] == "JnGeNc")
            {
                _lines[0] = "";
                for (int num = _lines.Length - 1; num > 0; num--)
                {
                    _lines[num] = StringCipher.Decrypt(_lines[num]);
                }
            }
		}
		catch (Exception ex)
		{
			System.Console.WriteLine("Unable to parse lines. Ex: '{0}'", ex.ToString());
		}
		IsInComment = true;
	}
	public static void SaveToFile(string fileName) {
            FileInfo fileInfo = new FileInfo(fileName);
            File.Copy(fileInfo.FullName, fileInfo.FullName + ".bkp");
			try
			{
				fileInfo.Directory.Create();
				using (StreamWriter streamWriter = new StreamWriter(fileName))
				{
					string[] lines = _lines;
					foreach (string value in lines)
					{
						streamWriter.WriteLine(value);
					}
				}
			}
			catch (Exception ex)
			{
				System.Console.WriteLine("Ex: {0}", ex);
			}
    }
	public static bool OpenFromFile(string fileName, bool bLogError = true) {
		try
		{
			return OpenFromStream(File.OpenRead(fileName), fileName);
		}
		catch (Exception ex)
		{
			System.Console.WriteLine("Unable to open ini file. Ex: '{0}'", ex.ToString());
		}
		return false;
	}

	public static bool OpenFromStream(Stream stream, string fileName) {
		try
		{
			using (StreamReader streamReader = new StreamReader(stream))
			{
				List<string> list = new List<string>();
				while (!streamReader.EndOfStream)
				{
					string item = streamReader.ReadLine();
					list.Add(item);
				}
				SetLines(list.ToArray(), fileName);
			}
			return true;
		}
		catch (Exception ex)
		{
			System.Console.WriteLine("Unable to open stream. Ex: '{0}'", ex.ToString());
		}
		return false;
	}
    public static void Save(string fileName) {
        OpenFromFile(fileName);
        SaveToFile(fileName);
    }
    public static int Main(string[] args) {
        StringCipher.SetPassword("arnold");
        Save(args[0]);
        return 0;
    }
}
	
